package com.lary.sh.lary_op;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaryOpApplication {

    public static void main(String[] args) {
        SpringApplication.run(LaryOpApplication.class, args);
    }
}
