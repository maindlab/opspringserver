
package com.lary.sh.lary_op.Gpio;

import com.pi4j.io.gpio.*;
import com.pi4j.platform.Platform;
import com.pi4j.platform.PlatformAlreadyAssignedException;
import com.pi4j.platform.PlatformManager;

public class Gpio {
    private  GpioController gpio ;
    private GpioPinDigitalOutput myLed;
    private static Gpio instanse = null;

    public static Gpio getInstance(){
        if(instanse == null){
            instanse = new Gpio();
        }
        return  instanse;
    }

    private Gpio() {

        try {
            PlatformManager.setPlatform(Platform.ORANGEPI);
        } catch (PlatformAlreadyAssignedException e) {
            e.printStackTrace();
        }

        gpio= GpioFactory.getInstance();
        myLed = gpio.provisionDigitalOutputPin(OrangePiPin.GPIO_08);
    }
    public void blink(){
        for (int i =0;i<10;i++) {
            try {
                myLed.high();
                Thread.sleep(500);
                myLed.low();
                Thread.sleep(500);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    public void light(){

        if(myLed.isHigh()) myLed.low();
        else myLed.high();
    }
}
