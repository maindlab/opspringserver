package com.lary.sh.lary_op.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class TestController {


    @RequestMapping("/hello")
    @ResponseBody // вернуть строк без нее вернет станицу
    public String hello(HttpServletResponse response, HttpServletRequest request ) {
        return "index.html";
    }

    @RequestMapping("/")
    //@ResponseBody // вернуть строк без нее вернет станицу
    public String index(HttpServletResponse response, HttpServletRequest request ) {
        return "index.html";
    }

}