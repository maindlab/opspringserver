package com.lary.sh.lary_op.Controllers;

import com.lary.sh.lary_op.Gpio.Gpio;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class GpioController {

    @RequestMapping("/blink")
    @ResponseBody // вернуть строк без нее вернет станицу
    public String blink(HttpServletResponse response, HttpServletRequest request )
    {
        Gpio gpio =Gpio.getInstance();
        gpio.blink();
        return "blink";
    }

    @RequestMapping("/light")
    @ResponseBody
    public void light(){
        Gpio gpio =Gpio.getInstance();
        gpio.light();
    }
}
