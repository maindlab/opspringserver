package com.lary.sh.lary_op.Controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyCustomError implements ErrorController {
    @RequestMapping("/error")
    public String error(){
        return "error.html";
    }
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
